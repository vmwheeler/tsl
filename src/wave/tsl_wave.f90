module tsl_wave

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  public :: wavelength_to_ang_frequency
  public :: wavelength_to_frequency
  public :: wavelength_to_ang_wavenumber
  public :: wavelength_to_wavenumber

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure function wavelength_to_ang_frequency(lambda, v) result (omega)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: lambda, v
      real (kind = k_r8)              :: omega
    end function wavelength_to_ang_frequency
  end interface

  interface
    pure function wavelength_to_frequency(lambda, v) result (nu)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: lambda, v
      real (kind = k_r8)              :: nu
    end function wavelength_to_frequency
  end interface

  interface
    pure function wavelength_to_ang_wavenumber(lambda) result (k)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: lambda
      real (kind = k_r8)              :: k
    end function wavelength_to_ang_wavenumber
  end interface

  interface
    pure function wavelength_to_wavenumber(lambda) result (eta)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: lambda
      real (kind = k_r8)              :: eta
    end function wavelength_to_wavenumber
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module tsl_wave
