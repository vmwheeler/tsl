pure function wavelength_to_frequency(lambda, v) result (nu)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_r8

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real (kind = k_r8), intent (in) :: lambda, v
  real (kind = k_r8)              :: nu

  ! -- Local declarations --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  nu = v / lambda

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end function wavelength_to_frequency
