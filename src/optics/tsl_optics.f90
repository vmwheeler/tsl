module tsl_optics

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: fresnel
  public :: snell

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure subroutine fresnel(mrel, theta, rho_per, rho_par)
      use calc, only: k_r8, k_c8
      implicit none
      complex (kind = k_c8), intent (in)  :: mrel
      real    (kind = k_r8), intent (in)  :: theta
      real    (kind = k_r8), intent (out) :: rho_per, rho_par
    end subroutine fresnel
  end interface

  interface
    pure function snell(mrel, theta1) result (theta2)
      use calc, only: k_r8, k_c8
      implicit none
      complex (kind = k_c8), intent (in) :: mrel
      real    (kind = k_r8), intent (in) :: theta1
      real    (kind = k_r8)              :: theta2
    end function snell
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module tsl_optics
