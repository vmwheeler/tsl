pure function snell(mrel, theta1) result (theta2)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_r8, k_c8

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  complex (kind = k_c8), intent (in) :: mrel
  real    (kind = k_r8), intent (in) :: theta1
  real    (kind = k_r8)              :: theta2

  ! -- Local declarations --

  real(k_r8) :: a, b, n, k, p

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  n = real(mrel, kind = k_r8)
  k = aimag(mrel)

  a = n * n - k * k - sin(theta1)**2
  b = (2 * n * k)**2

  p   = sqrt(0.5_k_r8 * (sqrt(a * a + b) + a)) 

  theta2 = atan(sin(theta1) / p)

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end function snell
