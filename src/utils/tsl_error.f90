subroutine tsl_error(string)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_ch

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  character (kind = k_ch, len = *), intent (in) :: string

  ! -- Local declarations --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  print '()'
  print '()'
  print '(80(''-''))'
  print '(25x, ''Thermal Science Library (TSL)'')'
  print '()'
  print '(2x, ''Error:'')'
  print '()'
  print *, string
  print '()'
  print '(2x, ''Program terminated by tsl_error.'')'
  print '(80(''-''))'
  print '()'
  print '()'

  ! -- Last executable statement --

  stop

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine tsl_error
