module tsl_utils

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_ch

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: tsl_footer
  public :: tsl_header
  public :: tsl_error
  public :: tsl_preamble
  public :: tsl_version
  public :: tsl_warning
  public :: tsl_year

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    subroutine tsl_footer()
      implicit none
    end subroutine tsl_footer
  end interface

  interface
    subroutine tsl_header()
      implicit none
    end subroutine tsl_header
  end interface

  interface
    subroutine tsl_error(string)
      use calc, only: k_ch
      implicit none
      character (kind = k_ch, len = *), intent (in) :: string
    end subroutine tsl_error
  end interface

  interface
    subroutine tsl_preamble
      implicit none
    end subroutine tsl_preamble
  end interface

  interface
    function tsl_version() result(version)
      use calc, only: k_ch
      implicit none
      character (kind = k_ch, len = 4) :: version
    end function tsl_version
  end interface

  interface
    subroutine tsl_warning(string)
      use calc, only: k_ch
      implicit none
      character (kind = k_ch, len = *), intent (in) :: string
    end subroutine tsl_warning
  end interface

  interface
    function tsl_year() result(year)
      use calc, only: k_ch
      implicit none
      character (kind = k_ch, len = 4) :: year
    end function tsl_year
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module tsl_utils
