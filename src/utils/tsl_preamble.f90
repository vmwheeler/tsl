subroutine tsl_preamble()

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use tsl_utils, only: tsl_version, tsl_year

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  ! -- Local declarations --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  print '()'
  print '()'
  print '(80(''-''))'
  print '(25x, ''Thermal Science Library (TSL)'')'
  print '(34x, ''Version'', a4, 1x)', tsl_version() 
  print '()'
  print '(26x, ''Copyright'', 1x, a4, 1x, ''The TSL Team'')', tsl_year()
  print '()'
  print '(2x, ''TSL is free software: you can redistribute it '         // &
        'and/or modify'')'
  print '(2x, ''it under the terms of the GNU Lesser General Public '   // &
        'License as published by'')'
  print '(2x, ''the Free Software Foundation, either version 3 of the ' // &
        'License, or'')'
  print '(2x, ''(at your option) any later version.'')'
  print '()'
  print '(2x, ''TSL is distributed in the hope that it will be useful,'')'
  print '(2x, ''but WITHOUT ANY WARRANTY; without even the implied '    // &
        'warranty of'')'
  print '(2x, ''MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  ' // &
        'See the'')'
  print '(2x, ''GNU Lesser General Public License for more details.'')'
  print '()'
  print '(2x, ''You should have received a copy of the GNU Lesser '     // &
        'General Public License'')'
  print '(2x, ''along with TSL.  If not, '                              // &
        'see <http://www.gnu.org/licenses/>.'')'
  print '(80(''-''))'
  print '()'
  print '()'

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine tsl_preamble
