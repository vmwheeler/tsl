module tsl_scat

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  public :: bhmie

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure subroutine bhmie(x, refrel, nang, s1, s2, qext, qsca, qback, gsca)
      use calc, only: k_i4, k_r8, k_c8
      implicit none
      real    (kind = k_r8),                      intent (in)  :: x
      complex (kind = k_c8),                      intent (in)  :: refrel
      integer (kind = k_i4),                      intent (in)  :: nang
      complex (kind = k_c8), dimension(2*nang-1), intent (out) :: s1, s2
      real    (kind = k_r8),                      intent (out) :: qext, qsca
      real    (kind = k_r8),                      intent (out) :: qback, gsca
    end subroutine bhmie
  end interface
  
  public :: bhcoat

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure subroutine bhcoat(x,y,rrefcore,rrefshell,qext,qsca,qback,gsca)
       use calc, only: k_i4, k_r8, k_c8, c_pio2
       implicit none
       real    (kind = k_r8),                      intent (in)  :: x, y
       complex (kind = k_c8),                      intent (in)  :: rrefcore
       complex (kind = k_c8),                      intent (in)  :: rrefshell
       real    (kind = k_r8),                      intent (out) :: qext, qsca
       real    (kind = k_r8),                      intent (out) :: qback, gsca
    end subroutine bhcoat
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module tsl_scat
