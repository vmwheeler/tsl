subroutine bhcoat(x,y,rrefcore,rrefshell,qext,qsca,qback,gsca)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 1983 Craig F. Bohren and Donald R. Huffman
! Copyright 2016 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_i4, k_r8, k_c8, c_pio2

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8),                      intent (in)  :: x, y
  complex (kind = k_c8),                      intent (in)  :: rrefcore
  complex (kind = k_c8),                      intent (in)  :: rrefshell
  real    (kind = k_r8),                      intent (out) :: qext, qsca
  real    (kind = k_r8),                      intent (out) :: qback, gsca

  ! -- Local declarations --
  
  !examples
  !real    (kind = k_r8), dimension (:), allocatable :: tau, theta
  !integer (kind = k_i4)                             :: j, jj, n, nmx, nn, nstop
  
  complex (kind = k_c8), parameter          :: ii = (0.0,1.D0) 
  real (kind = k_r8), parameter             :: del = 1.D-8  
  integer (kind = k_i4)                     :: iflag, n, nstop
  
  real (kind = k_r8)                        :: chi0y,chi1y,chiy,en
  real (kind = k_r8)                        :: psi0y,psi1y,psiy
  real (kind = k_r8)                        :: rn,ystop
  
  complex (kind = k_c8)                     :: amess1,amess2,amess3,amess4
  complex (kind = k_c8)                     :: an,an1,ancap
  complex (kind = k_c8)                     :: bn,bn1,bncap,brack
  complex (kind = k_c8)                     :: chi0x2,chi0y2,chi1x2,chi1y2
  complex (kind = k_c8)                     :: chix2,chipx2,chipy2,chiy2,crack
  complex (kind = k_c8)                     :: d0x1,d0x2,d0y2
  complex (kind = k_c8)                     :: d1x1,d1x2,d1y2
  complex (kind = k_c8)                     :: dnbar,gnbar, refrel
  complex (kind = k_c8)                     :: xback,xi0y,xi1y,xiy
  complex (kind = k_c8)                     :: x1,x2,y2

  
  x1=rrefcore*x
  x2=rrefshell*x
  y2=rrefshell*y
  ystop=y+4.*y**0.3333+2.0
  refrel=rrefshell/rrefcore
  nstop=ystop
  
!         -----------------------------------------------------------
!              series terminated after nstop terms
!         -----------------------------------------------------------
  d0x1=cos(x1)/sin(x1)
  d0x2=cos(x2)/sin(x2)
  d0y2=cos(y2)/sin(y2)
  psi0y=cos(y)
  psi1y=sin(y)
  chi0y=-sin(y)
  chi1y=cos(y)
  xi0y=psi0y-ii*chi0y
  xi1y=psi1y-ii*chi1y
  chi0y2=-sin(y2)
  chi1y2=cos(y2)
  chi0x2=-sin(x2)
  chi1x2=cos(x2)
  qsca=0.0
  qext=0.0
  xback=(0.0,0.0)
  iflag=0
  do n=1,nstop
      rn=n
      en=rn
      psiy=(2.0*rn-1.)*psi1y/y-psi0y
      chiy=(2.0*rn-1.)*chi1y/y-chi0y
      xiy=psiy-ii*chiy
      d1y2=1.0/(rn/y2-d0y2)-rn/y2
      if(iflag.eq.0)then

! calculate inner sphere ancap, bncap
!           and brack and crack

        d1x1=1.0/(rn/x1-d0x1)-rn/x1
        d1x2=1.0/(rn/x2-d0x2)-rn/x2
        chix2=(2.0*rn-1.0)*chi1x2/x2-chi0x2
        chiy2=(2.0*rn-1.0)*chi1y2/y2-chi0y2
        chipx2=chi1x2-rn*chix2/x2
        chipy2=chi1y2-rn*chiy2/y2
        ancap=refrel*d1x1-d1x2
        ancap=ancap/(refrel*d1x1*chix2-chipx2)
        ancap=ancap/(chix2*d1x2-chipx2)
        brack=ancap*(chiy2*d1y2-chipy2)
        bncap=refrel*d1x2-d1x1
        bncap=bncap/(refrel*chipx2-d1x1*chix2)
        bncap=bncap/(chix2*d1x2-chipx2)
        crack=bncap*(chiy2*d1y2-chipy2)

! calculate convergence test expressions for inner sphere
! see pp 483-485 of bohren & huffman for definitions

        amess1=brack*chipy2
        amess2=brack*chiy2
        amess3=crack*chipy2
        amess4=crack*chiy2

      endif ! test on iflag.eq.0

! now test for convergence for inner sphere
! all four criteria must be satisfied
! see p 484 of bohren & huffman

      if(abs(amess1).lt.del*abs(d1y2) &
         .and. abs(amess2) &
         .lt.del.and. abs(amess3) &
         .lt.del*abs(d1y2) &
         .and. abs(amess4).lt.del)then

! convergence for inner sphere

        brack=(0.,0.)
        crack=(0.,0.)
        iflag=1
      else

! no convergence yet

        iflag=0

      endif
      dnbar=d1y2-brack*chipy2
      dnbar=dnbar/(1.0-brack*chiy2)
      gnbar=d1y2-crack*chipy2
      gnbar=gnbar/(1.0-crack*chiy2)

! store previous values of an and bn for use in computation of 
! g=<cos(theta)>

      if(n.gt.1)then
        an1=an
        bn1=bn
      endif

! update an and bn

      an=(dnbar/rrefshell+rn/y)*psiy-psi1y
      an=an/((dnbar/rrefshell+rn/y)*xiy-xi1y)
      bn=(rrefshell*gnbar+rn/y)*psiy-psi1y
      bn=bn/((rrefshell*gnbar+rn/y)*xiy-xi1y)

! calculate sums for qsca,qext,xback

      qsca=qsca+(2.0*rn+1.0)*(abs(an)*abs(an)+abs(bn)*abs(bn))
      xback=xback+(2.0*rn+1.0)*(-1.)**n*(an-bn)
      qext=qext+(2.0*rn+1.0)*(dble(an)+dble(bn))

! (fsb) calculate the sum for the asymmetry factor

      gsca=gsca+((2.0*en+1.)/(en*(en+1.0))) &
            *(real(an)*real(bn)+aimag(an)*aimag(bn))
      if(n.gt.1)then
        gsca=gsca+((en-1.)*(en+1.)/en) &
            *(real(an1)*real(an)+aimag(an1)*aimag(an) &
            +real(bn1)*real(bn)+aimag(bn1)*aimag(bn))
      endif

! continue update for next iteration

      psi0y=psi1y
      psi1y=psiy
      chi0y=chi1y
      chi1y=chiy
      xi1y=psi1y-ii*chi1y
      chi0x2=chi1x2
      chi1x2=chix2
      chi0y2=chi1y2
      chi1y2=chiy2
      d0x1=d1x1
      d0x2=d1x2
      d0y2=d1y2
  enddo

! have summed sufficient terms
! now compute qqsca,qqext,qback, and gsca
  
  qsca=(2.0/(y*y))*qsca
  qext=(2.0/(y*y))*qext
  qback=(abs(xback))**2
  qback=(1.0/(y*y))*qback
  gsca=2.0*gsca/qsca

  
     
  return

end
