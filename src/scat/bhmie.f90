subroutine bhmie(x, refrel, nang, s1, s2, qext, qsca, qback, gsca)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 1983 Craig F. Bohren and Donald R. Huffman
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_i4, k_r8, k_c8, c_pio2

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8),                      intent (in)  :: x
  complex (kind = k_c8),                      intent (in)  :: refrel
  integer (kind = k_i4),                      intent (in)  :: nang
  complex (kind = k_c8), dimension(2*nang-1), intent (out) :: s1, s2
  real    (kind = k_r8),                      intent (out) :: qext, qsca
  real    (kind = k_r8),                      intent (out) :: qback, gsca

  ! -- Local declarations --

  complex (kind = k_c8)                             :: an, bn, an1, bn1
  complex (kind = k_c8)                             :: xi, xi1, y
  complex (kind = k_c8), dimension (:), allocatable :: d
  real    (kind = k_r8), dimension (:), allocatable :: amu, pi0, pi1, pi2
  real    (kind = k_r8), dimension (:), allocatable :: tau, theta
  real    (kind = k_r8)                             :: apsi, apsi1, dn, dx
  real    (kind = k_r8)                             :: psi, psi0, psi1
  real    (kind = k_r8)                             :: xstop, ymod
  real    (kind = k_r8)                             :: chi, chi0, chi1, dang
  real    (kind = k_r8)                             :: fn, rn, p, t
  integer (kind = k_i4)                             :: j, jj, n, nmx, nn, nstop

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  allocate (amu(nang), pi0(nang), pi1(nang), pi2(nang), tau(nang), theta(nang))
  dx = x
  y  = x*refrel
 
  ! Series terminated after nstop terms

  xstop = x + 4.0_k_r8 * x**0.3333 + 2.0_k_r8
  nstop = nint(xstop)
  ymod  = abs(y)
  nmx   = int(max(xstop, ymod)) + 15
  allocate (d(nmx))
  dang  = c_pio2 / real(nang - 1, k_r8)
  do j  = 1, nang
    theta(j) = (real(j, k_r8)-1.)*dang
    amu(j)   = cos(theta(j))
  end do

  ! Logarithmic derivative d(j) calculated by downward recurrence beginning
  ! with initial value 0.0 + i*0.0 at j = nmx

  d(nmx) = (0.0_k_r8, 0.0_k_r8)
  nn     = nmx-1
  do n = 1, nn
    rn       = real(nmx-n+1, k_r8)
    d(nmx - n) = (rn / y) - (1. / (d(nmx - n + 1) + rn / y))
  end do
  do j = 1, nang
    pi0(j) = 0.0_k_r8
    pi1(j) = 1.0_k_r8
  end do
  nn = 2 * nang - 1
  do j = 1, nn
    s1(j) =(0.0_k_r8, 0.0_k_r8)
    s2(j) =(0.0_k_r8, 0.0_k_r8)
  end do

  ! Riccati-Bessel functions with real argument x calculated by upward
  ! recurrence

  psi0  =  cos(dx)
  psi1  =  sin(dx)
  chi0  = -sin(x)
  chi1  =  cos(x)
  apsi1 = psi1
  xi1   = cmplx(apsi1, -chi1, k_c8)
  qsca  = 0.0_k_r8
  gsca  = 0.0_k_r8

!!! until here

  n = 1
  do
    dn = real(n, k_r8)
    rn = real(n, k_r8)
    fn = (2._k_r8*rn+1._k_r8)/(rn*(rn+1._k_r8))
    psi = (2._k_r8*dn-1._k_r8)*psi1/dx-psi0
    apsi = psi
    chi = (2._k_r8*rn-1._k_r8)*chi1/x-chi0
    xi = cmplx(apsi, -chi, k_c8)
    if (n .GT. 1) THEN
      an1 = an
      bn1 = bn
    end if
    an = (d(n)/refrel+rn/x)*apsi-apsi1
    an = an/((d(n)/refrel+rn/x)*xi-xi1)
    bn = (refrel*d(n)+rn/x)*apsi-apsi1
    bn = bn/((refrel*d(n)+rn/x)*xi-xi1)
    qsca = qsca+(2._k_r8*rn+1._k_r8)*(abs(an)*abs(an)+abs(bn)*abs(bn))
    gsca = gsca+(2._k_r8*rn+1._k_r8)/(rn*(rn+1._k_r8))*(real(an)*real(bn)+aimag(an)*aimag(bn))
    if (n .GT. 1) THEN
      gsca = gsca+(rn-1._k_r8)*(rn+1._k_r8)/rn*(real(an1, k_r8)*real(an, k_r8)+ &
                                                aimag(an1)*aimag(an)+ &
                                                real(bn1, k_r8)*real(bn, k_r8)+ &
                                                aimag(bn1)*aimag(bn))
    end if
    do j = 1, nang
      jj = 2*nang-j 
      pi2(j) = pi1(j)
      tau(j) = rn*amu(j)*pi2(j)-(rn+1.)*pi0(j)
      p = (-1._k_r8)**real(n-1, k_r8)
      s1(j) = s1(j)+fn*(an*pi2(j)+bn*tau(j))
      t = (-1._k_r8)**real(n, k_r8)
      s2(j) = s2(j)+fn*(an*tau(j)+bn*pi2(j))
      if(j /= jj) then
        s1(jj) = s1(jj)+fn*(an*pi2(j)*p+bn*tau(j)*t)
        s2(jj) = s2(jj)+fn*(an*tau(j)*t+bn*pi2(j)*p)
      end if
    end do
    psi0 = psi1
    psi1 = psi
    apsi1 = psi1
    chi0 = chi1
    chi1 = chi
    xi1 = cmplx(apsi1, -chi1, k_c8)
    n = n+1
    rn = real(n, k_r8)
    do j = 1, nang
      pi1(j) = ((2._k_r8*rn-1._k_r8)/(rn-1._k_r8))*amu(j)*pi2(j)
      pi1(j) = pi1(j)-rn*pi0(j)/(rn-1._k_r8)
      pi0(j) = pi2(j)
    end do
    if(n-1-nstop >= 0) exit
  end do
  gsca = 2._k_r8*gsca/qsca
  qsca = (2._k_r8/(x*x))*qsca
  qext = (4._k_r8/(x*x))*real(s1(1), k_r8)
  qback = (4._k_r8/(x*x))*abs(s1(2*nang-1))*abs(s1(2*nang-1))
  deallocate(d)
  deallocate(amu, pi0, pi1, pi2, tau, theta)
  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine bhmie
