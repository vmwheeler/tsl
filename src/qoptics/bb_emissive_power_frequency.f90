pure function bb_emissive_power_frequency(nu, t) result (eb)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_r8
  use calc, only: c_boltzmann, c_pi, c_planck, c_speed_light_vacuum

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real (kind = k_r8), intent (in) :: nu, t
  real (kind = k_r8)              :: eb

  ! -- Local declarations --

  real (kind = k_r8)             :: x

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  x  = c_planck * nu / (c_boltzmann * t)
  eb = 2.0_k_r8 * c_pi * c_planck * nu ** 3 / &
       (c_speed_light_vacuum ** 2 * (exp(x) - 1.0_k_r8))

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end function bb_emissive_power_frequency
