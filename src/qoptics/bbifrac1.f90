pure function bb_inverse_fraction_wavelength(f) result(lambdat)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_r8

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real (kind = k_r8), intent (in)      :: f
  real (kind = k_r8)                   :: lambdat

  ! -- Local declarations --

  real (kind = k_r8), dimension (6, 5) :: a
  real (kind = k_r8), dimension (3, 5) :: b

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  lambdat = 0.0_k_r8

  a =                                                          &
    reshape(                                                   &
      source =                                                 &
        [                                                      &
             503.247_k_r8,   1560.840_k_r8,   2846.63000_k_r8, &
          345197.000_k_r8,      1.200_k_r8,      1.10064_k_r8, &
             230.243_k_r8,   7603.610_k_r8,  -4430.38000_k_r8, &
        -1828567.000_k_r8,      9.476_k_r8,     16.81480_k_r8, &
            5863.850_k_r8, -15540.100_k_r8,  27936.00000_k_r8, &
         3674856.000_k_r8,    -44.840_k_r8,   -183.44500_k_r8, &
          -10759.600_k_r8,  31257.700_k_r8, -41041.90000_k_r8, &
        -3284391.000_k_r8,    156.900_k_r8,    890.69900_k_r8, &
            8723.140_k_r8, -20844.800_k_r8,  25960.90000_k_r8, &
         1108939.000_k_r8,      0.000_k_r8,      0.00000_k_r8  &
        ],                                                     &
      shape =                                                  &
        [6, 5]                                                 &
    )
  b =                                                          &
    reshape(                                                   &
      source =                                                 &
        [                                                      &
          0.000_k_r8, 0.000_k_r8, 1.000_k_r8, 0.125_k_r8,      &
          1.000_k_r8, 2.000_k_r8, 0.250_k_r8, 2.000_k_r8,      &
          3.000_k_r8, 0.375_k_r8, 3.000_k_r8, 4.000_k_r8,      &
          0.500_k_r8, 4.000_k_r8, 0.000_k_r8                   &
        ],                                                     &
      shape =                                                  &
        [3, 5]                                                 &
    )

  if (f > 0.0_k_r8 .and. f <= 0.1_k_r8) then
    lambdat = sum(a(1,:) * f ** b(1,:))
  else if (f > 0.1_k_r8 .and. f <= 0.4_k_r8) then
    lambdat = sum(a(2,:) * f ** b(2,:))
  else if (f > 0.4_k_r8 .and. f <= 0.7_k_r8) then
    lambdat = sum(a(3,:) * f ** b(2,:))
  else if (f > 0.7_k_r8 .and. f <= 0.9_k_r8) then
    lambdat = sum(a(4,:) * f ** b(2,:))
  else if (f > 0.9_k_r8 .and. f <= 0.99_k_r8) then
    lambdat = (                                            &
                0.152886e12_k_r8 /                         &
                sum(a(5,1:4) * (1.0_k_r8 - f) ** b(3,1:4)) &
              ) ** (1.0_k_r8/3.0_k_r8)
  else if (f > 0.99_k_r8 .and. f < 1.0_k_r8) then
    lambdat = (                                            &
                0.152886e12_k_r8 /                         &
                sum(a(6,1:4) * (1.0_k_r8 - f) ** b(3,1:4)) &
              ) ** (1.0_k_r8/3.0_k_r8)
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end function bb_inverse_fraction_wavelength
