module tsl_qoptics

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  public :: bb_emissive_power_ang_frequency
  public :: bb_emissive_power_frequency
  public :: bb_emissive_power_wavelength
  public :: bb_emissive_power_ang_wavenumber
  public :: bb_emissive_power_wavenumber

  public :: bb_energy_density_ang_frequency
  public :: bb_energy_density_frequency
  public :: bb_energy_density_wavelength
  public :: bb_energy_density_ang_wavenumber
  public :: bb_energy_density_wavenumber

  public :: bb_fraction_ang_frequency
  public :: bb_fraction_frequency
  public :: bb_fraction_wavelength
  public :: bb_fraction_ang_wavenumber
  public :: bb_fraction_wavenumber

  public :: bb_intensity_ang_frequency
  public :: bb_intensity_frequency
  public :: bb_intensity_wavelength
  public :: bb_intensity_ang_wavenumber
  public :: bb_intensity_wavenumber

  public :: bb_inverse_fraction_ang_frequency
  public :: bb_inverse_fraction_frequency
  public :: bb_inverse_fraction_wavelength
  public :: bb_inverse_fraction_ang_wavenumber
  public :: bb_inverse_fraction_wavenumber

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure function bb_emissive_power_ang_frequency(omega, t) result (eb)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: omega, t
      real (kind = k_r8)              :: eb
    end function bb_emissive_power_ang_frequency
  end interface

  interface
    pure function bb_emissive_power_frequency(nu, t) result (eb)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: nu, t
      real (kind = k_r8)              :: eb
    end function bb_emissive_power_frequency
  end interface

  interface
    pure function bb_emissive_power_wavelength(lambda, t) result (eb)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: lambda, t
      real (kind = k_r8)              :: eb
    end function bb_emissive_power_wavelength
  end interface

  interface
    pure function bb_emissive_power_ang_wavenumber(eta, t) result (eb)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: eta, t
      real (kind = k_r8)              :: eb
    end function bb_emissive_power_ang_wavenumber
  end interface
  interface

    pure function bb_emissive_power_wavenumber(k, t) result (eb)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: k, t
      real (kind = k_r8)              :: eb
    end function bb_emissive_power_wavenumber
  end interface

  interface
    pure function bb_energy_density_ang_frequency(omega, t) result (ub)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: omega, t
      real (kind = k_r8)              :: ub
    end function bb_energy_density_ang_frequency
  end interface

  interface
    pure function bb_energy_density_frequency(nu, t) result (ub)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: nu, t
      real (kind = k_r8)              :: ub
    end function bb_energy_density_frequency
  end interface

  interface
    pure function bb_energy_density_wavelength(lambda, t) result (ub)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: lambda, t
      real (kind = k_r8)              :: ub
    end function bb_energy_density_wavelength
  end interface

  interface
    pure function bb_energy_density_ang_wavenumber(k, t) result (ub)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: k, t
      real (kind = k_r8)              :: ub
    end function bb_energy_density_ang_wavenumber
  end interface

  interface
    pure function bb_energy_density_wavenumber(eta, t) result (ub)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: eta, t
      real (kind = k_r8)              :: ub
    end function bb_energy_density_wavenumber
  end interface

  interface
    pure function bb_fraction_ang_frequency(omegat, n_terms) result (f)
      use calc, only: k_i2, k_r8
      implicit none
      real (kind = k_r8), intent (in)    :: omegat
      integer (kind = k_i2), intent (in) :: n_terms
      real (kind = k_r8)                 :: f
    end function bb_fraction_ang_frequency
  end interface

  interface
    pure function bb_fraction_frequency(nut, n_terms) result (f)
      use calc, only: k_i2, k_r8
      implicit none
      real (kind = k_r8), intent (in)    :: nut
      integer (kind = k_i2), intent (in) :: n_terms
      real (kind = k_r8)                 :: f
    end function bb_fraction_frequency
  end interface

  interface
    pure function bb_fraction_wavelength(lambdat, n_terms) result (f)
      use calc, only: k_i2, k_r8
      implicit none
      real    (kind = k_r8), intent (in) :: lambdat
      integer (kind = k_i2), intent (in) :: n_terms
      real    (kind = k_r8)              :: f
    end function bb_fraction_wavelength
  end interface

  interface
    pure function bb_fraction_wavenumber(etat, n_terms) result (f)
      use calc, only: k_i2, k_r8
      implicit none
      real (kind = k_r8), intent (in)    :: etat
      integer (kind = k_i2), intent (in) :: n_terms
      real (kind = k_r8)                 :: f
    end function bb_fraction_wavenumber
  end interface

  interface
    pure function bb_fraction_ang_wavenumber(kt, n_terms) result (f)
      use calc, only: k_i2, k_r8
      implicit none
      real (kind = k_r8), intent (in)    :: kt
      integer (kind = k_i2), intent (in) :: n_terms
      real (kind = k_r8)                 :: f
    end function bb_fraction_ang_wavenumber
  end interface

  interface
    pure function bb_intensity_ang_frequency(omega, t) result (ib)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: omega, t
      real (kind = k_r8)              :: ib
    end function bb_intensity_ang_frequency
  end interface

  interface
    pure function bb_intensity_frequency(nu, t) result (ib)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: nu, t
      real (kind = k_r8)              :: ib
    end function bb_intensity_frequency
  end interface

  interface
    pure function bb_intensity_wavelength(lambda, t) result (ib)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: lambda, t
      real (kind = k_r8)              :: ib
    end function bb_intensity_wavelength
  end interface

  interface
    pure function bb_intensity_ang_wavenumber(k, t) result (ib)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: k, t
      real (kind = k_r8)              :: ib
    end function bb_intensity_ang_wavenumber
  end interface

  interface
    pure function bb_intensity_wavenumber(eta, t) result (ib)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: eta, t
      real (kind = k_r8)              :: ib
    end function bb_intensity_wavenumber
  end interface

  interface
    pure function bb_inverse_fraction_ang_frequency(f) result (omegat)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: f
      real (kind = k_r8)              :: omegat
    end function bb_inverse_fraction_ang_frequency
  end interface

  interface
    pure function bb_inverse_fraction_frequency(f) result (nut)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: f
      real (kind = k_r8)              :: nut
    end function bb_inverse_fraction_frequency
  end interface

  interface
    pure function bb_inverse_fraction_wavelength(f) result (lambdat)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: f
      real (kind = k_r8)              :: lambdat
    end function bb_inverse_fraction_wavelength
  end interface

  interface
    pure function bb_inverse_fraction_ang_wavenumber(f) result (kt)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: f
      real (kind = k_r8)              :: kt
    end function bb_inverse_fraction_ang_wavenumber
  end interface

  interface
    pure function bb_inverse_fraction_wavenumber(f) result (etat)
      use calc, only: k_r8
      implicit none
      real (kind = k_r8), intent (in) :: f
      real (kind = k_r8)              :: etat
    end function bb_inverse_fraction_wavenumber
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module tsl_qoptics
