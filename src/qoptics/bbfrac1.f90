pure function bb_fraction_wavelength(lambdat, n_terms) result (f)

!-------------------------------------------------------------------------------
! This file is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_i2, k_i4, k_r8
  use calc, only: c_pi, c_second_radiation

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8), intent (in) :: lambdat
  integer (kind = k_i2), intent (in) :: n_terms
  real    (kind = k_r8)              :: f

  ! -- Local declarations --

  real    (kind = k_r8), dimension (:), allocatable :: terms
  real    (kind = k_r8)                             :: x, x2, x3
  integer (kind = k_i2)                             :: i

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  allocate (terms(n_terms))

  f  = 0.0_k_r8
  x  = c_second_radiation / lambdat
  x2 = x  * x
  x3 = x2 * x

  forall (i = 1_k_i2:n_terms)
    terms(i) = exp(-real(i, kind = k_r8) * x) / real(i, kind = k_r8) * &
               (                                                       &
                 x3                                                  + &
                 3.0_k_r8 * x2 / real(i,         kind = k_r8)        + &
                 6.0_k_r8 * x  / real(i * i,     kind = k_r8)        + &
                 6.0_k_r8      / real(i * i * i, kind = k_r8)          &
               )
  end forall
 
  f = sum(terms) * 15.0_k_r8 / c_pi ** 4

  ! -- Last executable statement --

  deallocate (terms)

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------
 
end function bb_fraction_wavelength
