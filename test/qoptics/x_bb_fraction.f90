program x_bb_fraction

!-------------------------------------------------------------------------------
! This program is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_i2, k_r8
  use calc, only: c_third_radiation_wavelength
  use tsl,  only: bb_fraction_wavelength
  use tsl,  only: tsl_footer, tsl_header

  ! -- Null mapping --

  implicit none

  ! -- Declarations --

  real    (kind = k_r8) :: lambdat
  real    (kind = k_r8) :: f
  integer (kind = k_i2) :: n_terms

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  lambdat = c_third_radiation_wavelength
  n_terms = 15
  f       = bb_fraction_wavelength(lambdat, n_terms)

  call tsl_header()

  print '(2x, ''Usage example for function bb_fraction_wavelength'')'
  print '()'
  print '(2x, ''Wavelength-temperature product:'', f12.8, 1x, ''m*K'')', lambdat
  print '(2x, ''Blackbody fractional function: '', f12.8, 1x, ''''   )', f

  call tsl_footer()

  ! -- Last executable statement --

  stop

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end program x_bb_fraction
