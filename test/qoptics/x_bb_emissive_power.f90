program x_bb_emissive_power

!-------------------------------------------------------------------------------
! This program is part of the Thermal Science Library (TSL).
!
! Copyright 2012 The TSL Team
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_r8
  use calc, only: c_speed_light_vacuum
  use tsl,  only: bb_emissive_power_frequency
  use tsl,  only: bb_emissive_power_wavelength
  use tsl,  only: bb_emissive_power_wavenumber
  use tsl,  only: wavelength_to_frequency
  use tsl,  only: wavelength_to_wavenumber
  use tsl,  only: tsl_footer, tsl_header

  ! -- Null mapping --

  implicit none

  ! -- Declarations --

  real (kind = k_r8) :: eta, lambda, nu, t
  real (kind = k_r8) :: eb

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  lambda = 2.9e-6_k_r8
  t      = 1000.0_k_r8

  call tsl_header()

  eb     = bb_emissive_power_wavelength(lambda, t)

  print '(2x, ''Usage example for function bb_emissive_power_wavelength'')'
  print '()'
  print '(2x, ''Wavelength:              '', f12.7, 1x, ''m''       )', lambda
  print '(2x, ''Temperature:             '', f12.1, 1x, ''K''       )', t
  print '(2x, ''Spectral emissive power: '', e12.6, 1x, ''W/(m**3)'')', eb

  call tsl_footer()

  nu     = wavelength_to_frequency(lambda, c_speed_light_vacuum)
  eb     = bb_emissive_power_frequency(nu, t)

  print '(2x, ''Usage example for function bb_emissive_power_frequency'')'
  print '()'
  print '(2x, ''Frequency:               '', e12.6, 1x, ''1/s''       )', nu
  print '(2x, ''Spectral emissive power: '', e12.6, 1x, ''W*s/(m**2)'')', eb

  call tsl_footer()

  eta    = wavelength_to_wavenumber(lambda)
  eb     = bb_emissive_power_wavenumber(eta, t)

  print '(2x, ''Usage example for function bb_emissive_power_wavenumber'')'
  print '()'
  print '(2x, ''Spectroscopic wavenumber:'', f12.1, 1x, ''1/m''       )', eta
  print '(2x, ''Spectral emissive power: '', e12.6, 1x, ''W*m/(m**2)'')', eb

  call tsl_footer()

  ! -- Last executable statement --

  stop

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end program x_bb_emissive_power
