      PROGRAM CALLBHCOAT
      IMPLICIT NONE
! Local variables:
      INTEGER INPIND
      REAL PI,RADCOR,RADMAN,REFMED,WAVEL
      COMPLEX EPSCOR,EPSMAN
! Variables passed to subroutine:
      REAL GSCA,QABS,QBACK,QEXT,QSCA,XCOR,XMAN
      COMPLEX REFCOR,REFMAN
!***********************************************************************
! Program to compute optical properties of coated spheres using
! subroutine BHCOAT from Bohren & Huffman
!
! History:
! 90.10.11 (BTD) Modified structure of CALLBHCOAT
! 92.11.24 (BTD) Explicit declaration of all variables
! 00.05.16 (BTD) Modified to compile under g77
! 12.06.15 (BTD) Modified to use new version bhcoat_v2.f that
!                calculates <cos(theta)> for scattered power
! end history
!
!***********************************************************************
!
! Caution:
!     BHCOAT should not be used for large, highly absorbing coated
!     spheres.
!     x*refim1, x*refim2, and y*refim2 should be less than about 30.
!
! REFMED = (real) refractive index of surrounding medium
!***********************************************************************
      PI=4.*ATAN(1.)
      REFMED=1.0
! Specify whether to supply refractive index by hand or to obtain it
! using subroutine INDEX
      WRITE(*,*)'Wish to specify ref. indices (0) or diel.consts (1)?'
      READ(*,*)INPIND
 0050 WRITE(*,*)'Enter radius of core (physical units (0 to stop)'
      READ(*,*)RADCOR
      IF(RADCOR.LE.0.)STOP
      WRITE(*,*)'Enter radius of mantle (physical units)'
      READ(*,*)RADMAN
 0100 WRITE(*,*)'Enter wavelength (physical units) (0 to change size)'
      READ(*,*)WAVEL
      IF(WAVEL.LE.0.)GOTO 0050
      IF(INPIND.EQ.0)THEN
         WRITE(*,*)'Enter complex refractive index of core'
         READ(*,*)REFCOR
         WRITE(*,*)'Enter complex refractive index of mantle'
         READ(*,*)REFMAN
      ENDIF
      IF(INPIND.EQ.1)THEN
         WRITE(*,*)'Enter complex dielec. constant of core'
         READ(*,*)EPSCOR
         WRITE(*,*)'Enter complex dielec. constant of mantle'
         READ(*,*)EPSMAN
         REFCOR=SQRT(EPSCOR)
         REFMAN=SQRT(EPSMAN)
      ENDIF
      XCOR=2.*PI*RADCOR*REFMED/WAVEL
      XMAN=2.*PI*RADMAN*REFMED/WAVEL
      CALL BHCOAT(XCOR,XMAN,REFCOR,REFMAN,QEXT,QSCA,QBACK,GSCA)
      QABS=QEXT-QSCA
      WRITE(0,6010)RADCOR,RADMAN,WAVEL,XMAN,REFCOR,REFMAN,QEXT,QSCA,
     &             QABS,QBACK,GSCA
      GOTO 0100

 6010 FORMAT(
     &  1PE11.4,' = radius of core',/,
     &  1PE11.4,' = outer radius of mantle',/,
     &  1PE11.4,' = lambda',/,
     &  1PE11.4,' = x = 2*pi*rmantle/lambda',/,
     &  1P2E11.4,' = refractive index of core',/,
     &  1P2E11.4,' = refractive index of mantle',/,/,
     &  1PE11.4,' = Q_ext',/,
     &  1PE11.4,' = Q_sca',/,
     &  1PE11.4,' = Q_abs',/,
     &  1PE11.4,' = Q_bk=4*pi*dQ_sca/dOmega',/,
     &  1PE11.4,' = g = <cos(theta)>')
      END
