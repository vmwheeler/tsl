%-------------------------------------------------------------------------------
% This file is part of the Thermal Science Library (TSL).
%
% Copyright 2011 The TSL Team
%
% TSL is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% TSL is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with TSL.  If not, see <http://www.gnu.org/licenses/>.
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Begin identification, option declaration, and option execution part
%-------------------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tslbook}
\DeclareOption*{\PassOptionToClass{\CurrentOption}{book}}
\ProcessOptions

%-------------------------------------------------------------------------------
% End identification, option declaration, and option execution part
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Begin special class file commands
%-------------------------------------------------------------------------------

\LoadClass[10pt,openany,twoside]{book}

%-------------------------------------------------------------------------------
% End special class file commands
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Begin package loading part
%-------------------------------------------------------------------------------

\RequirePackage{amsmath}
\RequirePackage{array}
\RequirePackage[english]{babel}
\RequirePackage[b5paper, pdftex, top=22mm, bottom=20mm, left=20mm, right=20mm,
            footskip=7.5mm, headsep=7.5mm]{geometry}
\RequirePackage{fancyhdr}
\RequirePackage{graphicx}
\RequirePackage{color}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{SIunits}
\RequirePackage{textcomp}
\RequirePackage{titlesec}
\RequirePackage{caption}
\RequirePackage{wltex}

%-------------------------------------------------------------------------------
% End package loading part
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Begin main code part
%-------------------------------------------------------------------------------

% -- Set space parameters --

\renewcommand{\arraystretch}{1.5}

% -- Define column types --

\newcolumntype{D}{p{0.15\textwidth}p{0.85\textwidth}}
\newcolumntype{T}{%
  >{\raggedright\ttfamily\small}%
  p{0.15\textwidth}>{\small}p{0.15\textwidth}>{\small}p{0.5\textwidth}%
}

% -- Set page style --

% -- Define new commands --

\newcommand{\tablehead}{%
  {\normalfont\scshape\small Parameter} & 
  {\scshape\small Symbol} &
  {\scshape\small Description}  \\%
}

% -- Redefine '\maketitle' command --

% -- Redefine section title styles and spacing --

\titleformat{\subsection}[block]%
  {\normalsize\bfseries\sffamily}{}{0mm}{\MakeUppercase}%
  [\hrule height 0.0pt \relax]
\titleformat{\subsubsection}[block]%
  {\normalsize\scshape}{}{0mm}{}%
  [\hrule height 0.0pt \relax]
\titlespacing{\subsubsection}{0mm}{0.5\baselineskip}{0.1\baselineskip}

% -- Redefine enumaration labels --

% -- Setup figure captions --

\captionsetup{font=small}
\captionsetup[figure]{labelfont=bf}

%-------------------------------------------------------------------------------
% End main code part
%-------------------------------------------------------------------------------
